<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Rewrite Intelipost
 * @category    Avanti
 * @package     Avanti_RewriteIntelipost
 *
 * @copyright   Copyright (c) 2020 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\RewriteIntelipost\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class System
 */
class System extends AbstractHelper
{
    /**
     * @return bool
     */
    public function isEnabled() : bool
    {
        return (bool)$this->scopeConfig->getValue(
            Config::ENABLE_LOGS,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return bool
     */
    public function isMageLogs() : bool
    {
        return (bool)$this->scopeConfig->getValue(
            Config::IN_MAGE_LOG,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getNameLog() : string
    {
        return (string)$this->scopeConfig->getValue(
            Config::NAME_FILE_LOG,
            ScopeInterface::SCOPE_STORE
        );
    }
}