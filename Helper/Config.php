<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Rewrite Intelipost
 * @category    Avanti
 * @package     Avanti_RewriteIntelipost
 *
 * @copyright   Copyright (c) 2020 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\RewriteIntelipost\Helper;

/**
 * Class Config
 */
class Config
{
    const ENABLE_LOGS = 'rewriteintelipost/general/enable_logs';
    const IN_MAGE_LOG = 'rewriteintelipost/general/in_mage_log';
    const NAME_FILE_LOG = 'rewriteintelipost/general/file_name';
    const LOG_DIR = 'var/log/';
    const QUOTE_BY_PRODUCT = 'quote_by_product/';
    const QUOTE_BUSINESS_DAYS = 'quote/business_days/';
    const QUOTE_AVAILABLE_SCHEDULING_DATES = 'quote/available_scheduling_dates/';
    const EVENT = 'intelipost_dynamic_zipcode';
}