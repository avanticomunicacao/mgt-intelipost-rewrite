<?php

declare(strict_types=1);

namespace Avanti\RewriteIntelipost\Rewrite\Intelipost\Quote\Helper;

use Avanti\RewriteIntelipost\Helper\Config;
use Avanti\RewriteIntelipost\Helper\System;
use Intelipost\Basic\Helper\Api as intelipostBasicHelperApi;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface;

class Api extends intelipostBasicHelperApi
{

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var CacheInterface
     */
    protected $_cache;

    /**
     * @var System
     */
    private $system;

    /**
     * Api constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param CacheInterface $cache
     * @param LoggerInterface $logger
     * @param System $system
     */
    public function __construct
    (
        ScopeConfigInterface $scopeConfig,
        CacheInterface $cache,
        LoggerInterface $logger,
        System $system
    ) {
        $this->system = $system;
        $this->_scopeConfig = $scopeConfig;
        $this->_cache = $cache;
        $this->_logger = $logger;
        if (!$this->system->isMageLogs()) {
            $this->_logger->pushHandler(new StreamHandler(str_replace('/', DIRECTORY_SEPARATOR, Config::LOG_DIR.$this->system->getNameLog())));
        }
        parent::__construct($scopeConfig);
    }

    /**
     * @param $httpMethod
     * @param $apiMethod
     * @param bool $postData
     * @return array[]|mixed
     * @throws \Exception
     */
    public function quoteRequest($httpMethod, $apiMethod, &$postData = false)
    {
        $postData ['api_request'] = json_encode($postData);
        $result = null; // $this->getCache ($postData); // Disabled

        try {
            $response = $this->apiRequest($httpMethod, $apiMethod, json_encode($postData));

            if ($response) {
                $result = json_decode($response, true);
                if (!$result) {
                    $result = $this->getContingencyValues($postData);
                    $postData ['api_response'] = json_encode($result, true);
                    return $result;
                }
                if (!strcmp($result ['status'], 'ERROR')) {
                    $messages = null;
                    foreach ($result ['messages'] as $_message) {
                        $messages .= $_message ['text'];
                    }

                    if ($this->_scopeConfig->getValue('rewriteintelipost/general/enable_logs')) {
                        $this->_logger->debug($postData ['destination_zip_code'] . ' : ' . $messages);
                    }

                    throw new \Exception ($messages);
                }
                $postData ['api_response'] = $response;
                return $result;
            }
            throw new \Exception ("Não Foi possível calcular o frete.");
        } catch (\Exception $e) {
            $this->_logger->debug("Nao foi possível calcular o frete. Erro: " . $e);
            throw new \Exception ("Não Foi possível calcular o frete. Erro: " . $e->getMessage());
        }
    }

    /**
     * @param $postData
     * @return string
     */
    public function getCacheIdentifier($postData)
    {
        $identifier = 'intelipost_api_'
            . $postData ['destination_zip_code'] . '_'
            . $postData ['cart_weight'] . '_'
            . $postData ['cart_amount'] . '_'
            . $postData ['cart_qtys'];

        return $identifier;
    }

    /**
     * @param $postData
     * @return mixed
     */
    public function getCache($postData)
    {
        $identifier = $this->getCacheIdentifier($postData);
        $result = unserialize($this->_cache->load($identifier));
        return $result;
    }

    /**
     * @param $postData
     * @param $responseData
     * @return bool
     */
    public function saveCache($postData, $responseData)
    {
        $identifier = $this->getCacheIdentifier($postData);
        $lifetime = intval($this->_scopeConfig->getValue('carriers/intelipost/cache_exp_time'));
        $result = $this->_cache->save(serialize($responseData), $identifier, array('collections'), $lifetime);
        return $result;
    }

    /**
     * @param $postData
     * @return array[]
     */
    public function getContingencyValues($postData)
    {
        $destZipcode = intval($postData ['destination_zip_code']);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $dir = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
        $varPath = $dir->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR);
        $intelipostVarPath = $varPath . DIRECTORY_SEPARATOR . 'intelipost';
        // calculate State Codification
        $stateFile = $intelipostVarPath . DIRECTORY_SEPARATOR . 'state_codification.json';
        $stateContent = json_decode(file_get_contents($stateFile), true);
        $stateCode = null;
        $stateType = null;
        foreach ($stateContent [0] as $beginZip => $child) {
            if ($destZipcode >= $beginZip && $destZipcode <= $child ['cep_end']) {
                $stateCode = strtoupper($child ['state']);
                $stateType = strtoupper($child ['type']);
                break;
            }
        }
        // calculate Contingency Table
        $contingencyTable = $this->_scopeConfig->getValue('carriers/intelipost/contingency_table');
        $tableFile = $intelipostVarPath . DIRECTORY_SEPARATOR . $contingencyTable;
        $tableContent = json_decode(file_get_contents($tableFile), true);
        $totalWeight = 0;
        foreach ($postData ['products'] as $product) {
            $totalWeight += intval($product ['weight']);
        }
        $delivery = array(
            'delivery_method_id' => 'fallback',
            'delivery_method_type' => 'fallback',
            'provider_shipping_cost' => 0,
        );
        foreach ($tableContent as $stateId => $stateContent) {
            if (!strcmp(strtoupper($stateId), $stateCode)) {
                foreach ($stateContent as $regionId => $regionContent) {
                    if (!strcmp(strtoupper($regionId), $stateType)) {
                        $delivery ['delivery_estimate_business_days'] = $regionContent ['delivery_estimate_business_days'];

                        foreach ($regionContent ['final_shipping_cost'] as $weight => $price) {
                            if ($totalWeight <= $weight) {
                                $delivery ['final_shipping_cost'] = $price;
                                $delivery ['logistic_provider_name'] = $regionId;
                                $delivery ['description'] = $regionId;
                                $delivery ['delivery_method_name'] = $regionId;
                                break;
                            }
                        }
                    }
                }
            }
        }
        $result = array(
            'content' => array(
                'id' => 0,
                'delivery_options' => array($delivery)
            )
        );
        return $result;
    }

    /**
     * @param $originZipcode
     * @param $destPostcode
     * @param $businessDays
     * @return mixed
     */
    public function getEstimateDeliveryDate($originZipcode, $destPostcode, $businessDays)
    {
        $response = $this->apiRequest(self::GET, Config::QUOTE_BUSINESS_DAYS
            . "{$originZipcode}/{$destPostcode}/{$businessDays}");
        $result = json_decode($response, true);
        return $result;
    }

    /**
     * @param $originZipcode
     * @param $destPostcode
     * @param $deliveryMethodId
     * @return mixed
     */
    public function getAvailableSchedulingDates($originZipcode, $destPostcode, $deliveryMethodId)
    {
        $response = $this->apiRequest(self::GET, Config::QUOTE_AVAILABLE_SCHEDULING_DATES
            . "{$deliveryMethodId}/{$originZipcode}/{$destPostcode}");
        $result = json_decode($response, true);
        return $result;
    }
}

