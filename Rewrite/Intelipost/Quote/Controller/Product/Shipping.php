<?php
/*
 * @package     Intelipost_Quote
 * @copyright   Copyright (c) 2016 Gamuza Technologies (http://www.gamuza.com.br/)
 * @author      Eneias Ramos de Melo <eneias@gamuza.com.br>
 */

namespace Avanti\RewriteIntelipost\Rewrite\Intelipost\Quote\Controller\Product;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable;

class Shipping extends \Magento\Framework\App\Action\Action
{

    protected $_quote;

    protected $_resultPageFactory;

    protected $configurableProduct;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Quote\Model\Quote $quote,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        Configurable  $configurableProduct
    )
    {
        $this->_quote = $quote;
        $this->_resultPageFactory = $resultPageFactory;
        $this->configurableProduct = $configurableProduct;

        parent::__construct ($context);
    }

    public function execute()
    {
        $country   = $this->getRequest()->getParam('country');
        $postcode  = $this->getRequest()->getParam('postcode');
        $productId = $this->getRequest()->getParam('product');
        $qty       = $this->getRequest()->getParam('qty');

        $this->_quote->getShippingAddress()
            ->setCountryId($country)
            ->setPostcode($postcode)
            ->setCollectShippingRates(true);

        $product = $this->getProductById ($productId);

        $options = new \Magento\Framework\DataObject;
        $options->setProduct($product->getId());
        $options->setQty($qty);

        if (!strcmp ($product->getTypeId (), 'configurable'))
        {
            $superAttribute = $this->getRequest()->getParam('super_attribute');
            $options->setSuperAttribute($superAttribute);
            $simpleProduct = $this->configurableProduct->getProductByAttributes($superAttribute, $product);
            $productId = $simpleProduct->getId();
        }

        else if (!strcmp ($product->getTypeId (), 'bundle'))
        {
            $bundleOption    = $this->getRequest()->getParam('bundle_option');
            $bundleOptionQty = $this->getRequest()->getParam('bundle_option_qty');

            $options->setBundleOption ($bundleOption);
            $options->setBundleOptionQty ($bundleOptionQty);
        }

        $result = $this->_quote->addProduct($product, $options);
        if (empty($result)) die (__($result));

        $this->_quote->collectTotals();
        $result = $this->_quote->getShippingAddress()->getGroupedAllShippingRates();
        if (is_string ($result)) die ($result);

        $product = $this->getProductById($productId);
        $leadTimeProduct = intval($product->getLeadTime());

        foreach ($result as $carrier) {
            foreach ($carrier as $method) {
                try {
                    $methodTitle = explode(" ", $method->getMethodTitle());
                    foreach ($methodTitle as $key => $value) {
                        if (is_numeric($value)) {
                            $positionDays = $key;
                        }
                    }

                    $days = intval($methodTitle[$positionDays]);
                    $days = $days + intval($leadTimeProduct);

                    $nameMethod = null;
                    foreach ($methodTitle as $key => $value) {
                        if ($key == $positionDays) {
                            $nameMethod .= $days . " ";
                        } else {
                            $nameMethod .= $value . " ";
                        }
                    }
                    $method->setMethodTitle($nameMethod);
                } catch (\Exception $e) {
                    continue;
                }
            }
        }

        $resultPage = $this->_resultPageFactory->create();
        $this->getResponse()->setBody(
            $resultPage->getLayout()
                ->createBlock('Magento\Framework\View\Element\Template')
                ->setRates($result)
                ->setTemplate('Intelipost_Quote::product/view/result.phtml')
                ->toHtml()
        );
    }

    public function getProductById ($id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productManager = $objectManager->create('Magento\Catalog\Model\Product');

        return $productManager->load ($id);
    }
}