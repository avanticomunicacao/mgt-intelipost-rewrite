<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Avanti\RewriteIntelipost\Rewrite\Intelipost\Tracking\Controller\Webhook;

use Intelipost\Quote\Model\Resource\Shipment\CollectionFactory;
use Intelipost\Quote\Model\Shipment;
use Intelipost\Tracking\Controller\Webhook\Webhook as WebhookCore;
use Intelipost\Tracking\Helper\Data as TrackingHelper;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\InventoryApi\Api\GetSourcesAssignedToStockOrderedByPriorityInterface;
use Magento\InventoryCatalogApi\Api\DefaultSourceProviderInterface;
use Magento\InventorySalesApi\Model\StockByWebsiteIdResolverInterface;
use Magento\Sales\Api\Data\ShipmentExtensionFactory;
use Magento\Sales\Model\Convert\Order as ConvertOrder;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Shipment\TrackFactory;
use Psr\Log\LoggerInterface;

class Webhook extends WebhookCore
{
    protected $logger;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        TrackingHelper $helper,
        Context $context,
        CollectionFactory $collectionFactory,
        Shipment $shipment,
        Order $order,
        ConvertOrder $convertOrder,
        StockByWebsiteIdResolverInterface $stockByWebsiteIdResolver,
        GetSourcesAssignedToStockOrderedByPriorityInterface $getSourcesAssignedToStockOrderedByPriority,
        DefaultSourceProviderInterface $defaultSourceProvider,
        ShipmentExtensionFactory $shipmentExtensionFactory,
        TrackFactory $track,
        LoggerInterface $logger)
    {
        $this->logger = $logger;
        parent::__construct($scopeConfig, $helper, $context, $collectionFactory, $shipment, $order, $convertOrder, $stockByWebsiteIdResolver, $getSourcesAssignedToStockOrderedByPriority, $defaultSourceProvider, $shipmentExtensionFactory, $track);
    }


    public function execute()
    {
        $webhook_enabled = $this->_scopeConfig->getValue('carriers/intelipost_tracking/webhook_enabled');
        $config_api_key = $this->_scopeConfig->getValue('intelipost_basic/settings/api_key');
        $track_pre_ship = $this->_scopeConfig->getValue('carriers/intelipost_tracking/track_pre_ship');
        $status_created = $this->_scopeConfig->getValue('carriers/intelipost_tracking/status_created');
        $status_ready_for_shipment = $this->_scopeConfig->getValue('carriers/intelipost_tracking/status_ready_for_shipment');
        $status_shipped = $this->_scopeConfig->getValue('carriers/intelipost_tracking/status_shipped');
        $track_post_ship = $this->_scopeConfig->getValue('carriers/intelipost_tracking/track_post_ship');
        $status_in_transit = $this->_scopeConfig->getValue('carriers/intelipost_tracking/status_in_transit');
        $status_to_be_delivered = $this->_scopeConfig->getValue('carriers/intelipost_tracking/status_to_be_delivered');
        $status_delivered = $this->_scopeConfig->getValue('carriers/intelipost_tracking/status_delivered');
        $status_clarify_delivery_failed = $this->_scopeConfig->getValue('carriers/intelipost_tracking/status_clarify_delivery_failed');
        $status_delivery_failed = $this->_scopeConfig->getValue('carriers/intelipost_tracking/status_delivery_failed');
        $create_shipment_after_ip_shipped = $this->_scopeConfig->getValue('carriers/intelipost_tracking/create_shipment_after_ip_shipped');

        $pre_dispatch_events = ['NEW', 'READY_FOR_SHIPPING', 'SHIPPED'];
        $post_dispatch_events = ['TO_BE_DELIVERED', 'IN_TRANSIT', 'DELIVERED', 'CLARIFY_DELIVERY_FAIL', 'DELIVERY_FAILED'];

        if ($webhook_enabled) {
            try {
                $api_key = $this->getRequest()->getHeader('api-key');
                $this->logger->error("api key intelipost webhook: " . $api_key);
                if ($api_key == $config_api_key) {
                    $obj = json_decode(utf8_encode(file_get_contents('php://input')));
                    $increment_id = $obj->order_number;
                    $this->logger->error("increment id intelipost webhook: " . $increment_id);
                    $state = $obj->history->shipment_order_volume_state;
                    $this->logger->error("state intelipost webhook: " . $state);
                    $tracking_code = $obj->tracking_code;
                    $this->logger->error("tracking code intelipost webhook: " . $tracking_code);
                    $comment = '[Intelipost Webhook] - ' . $obj->history->shipment_volume_micro_state->default_name;

                    $shipmentObj = $this->_collectionFactory->create();
                    $shipmentObj->addFieldToFilter('so.increment_id', $increment_id);
                    $colData = $shipmentObj->getData();

                    if (is_array($colData)) {
                        $this->logger->error("result colllection shipmntObj webhook:");
                        $this->logger->error(print_r($colData));
                    }

                    if (!array_key_exists(0, $colData)) {
                        throw new \Exception("O array de shipment veio vazio.");
                    }

                    $orderId = $colData[0]['entity_id'];

                    $this->updateTrackingCode($colData, $tracking_code);

                    if ((in_array($state, $pre_dispatch_events) && $track_pre_ship)
                        || in_array($state, $post_dispatch_events) && $track_post_ship) {
                        switch (strtoupper($state)) {
                            case 'NEW':
                                $status = $status_created;
                                $this->updateOrder($orderId, $status, $comment);
                                break;

                            case 'READY_FOR_SHIPPING':
                                $status = $status_ready_for_shipment;
                                $this->updateOrder($orderId, $status, $comment);
                                break;

                            case 'SHIPPED':
                                $status = $status_shipped;
                                if ($create_shipment_after_ip_shipped) {
                                    $this->createShipment($orderId);
                                }
                                $this->updateOrder($orderId, $status, $comment);
                                break;

                            case 'IN_TRANSIT':
                                $status = $status_in_transit;
                                $this->updateOrder($orderId, $status, $comment);
                                break;

                            case 'TO_BE_DELIVERED':
                                $status = $status_to_be_delivered;
                                $this->updateOrder($orderId, $status, $comment);
                                break;

                            case 'DELIVERED':
                                $status = $status_delivered;
                                $this->updateOrder($orderId, $status, $comment);
                                break;

                            case 'CLARIFY_DELIVERY_FAIL':
                                $status = $status_clarify_delivery_failed;
                                $this->updateOrder($orderId, $status, $comment);
                                break;

                            case 'DELIVERY_FAILED':
                                $status = $status_delivery_failed;
                                $this->updateOrder($orderId, $status, $comment);
                                break;
                        }
                    }
                }
            } catch (\Exception $e) {
                $this->logger->error("Erro no webhoock da intelipost. Mensagem de erro: ");
                $this->logger->error($e->getMessage());
            }
        }
    }
}