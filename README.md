# Avanti RewriteIntelipost
    Utilizado para substituir o source_zip do módulo da intelipost pelo Zip/Postal Code default do Magento, para utilizar, basta instalar e habilitá-lo nas configurações. 

# Patchs
     - 1.0.5
     Using standard Magento 2 logs for Intelipost interaction
     Intelipost.log not more created
     Fixed bugs in intelipost messages logs 

## Configuration Path
    Stores > Configuration > Avanti > Rewrite Intelipost > Configurations

